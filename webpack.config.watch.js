/**
 * Created by gunmaden on 02.03.17.
 */

const webpack = require('webpack');
const BrowserSyncPlugin = require('browsersync-webpack-plugin');

module.exports = {
    output: {
        pathinfo: true,
        publicPath: 'localhost:3000' + '/',
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new BrowserSyncPlugin({
            target: process.env.DEVURL || 'http://localhost:3000',
            proxyUrl: 'http://localhost:3000',
            watch: true,
            delay: 500,
        }),
    ],
};
