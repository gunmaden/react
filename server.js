/**
 * Created by gunmaden on 14.02.17.
 */

let	express	= require('express');
let proxy = require('http-proxy-middleware');
let path = require('path');
const morgan = require('morgan');
const open = require('open');


let menuItems = ['/wars', '/forum', '/dialogs', '/users/*'];
let options = {
    target:'http://localhost:2403/',
    changeOrigin: true,
    pathRewrite:{
        '^/api/':'/'
    }
};

let	app	= express();

app.use('/', express.static(__dirname));

app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

app.get(menuItems, function (request, response){
    response.sendFile(path.resolve(__dirname, 'index.html'))
});



app.use('/api',proxy(options));
app.listen(3000);
open('http://localhost:3000', function (err) {
    if (err) throw err;
    console.log('The user closed the browser');
});
