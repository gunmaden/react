/**
 * Created by gunmaden on 14.02.17.
 */
import 'jquery'
import 'bootstrap'
import React from 'react'
import ReactDOM from 'react-dom'
import {Router, Link, Route, IndexRoute, IndexLink, browserHistory} from 'react-router'
import {News} from './News/news.js'
import {Wars} from './Wars/wars.js'
import {Users} from './Users/users.js'


var App = React.createClass({
    openNav: function () {
        document.getElementById("sideNav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.getElementById("body").style.opacity = "0.3";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    },
    closeNav: function () {
        document.getElementById("sideNav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
        document.getElementById("body").style.opacity = "1";
        document.body.style.backgroundColor = "white";
    },
    render: function () {
        return (
            <div className="app">
                <div id="sideNav" className="sideNav">
                    <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
                    <a href="#">About</a>
                    <a href="#">Services</a>
                    <a href="#">Clients</a>
                    <a href="#">Contact</a>
                </div>

                <div id="menu" className="constNav">
                    <i className="material-icons md-48" onClick={this.openNav}>menu</i>
                    <span><IndexLink to="/">News</IndexLink></span>
                    <span><Link to="/wars">Wars</Link></span>
                    <span><Link to="/forum">Forum</Link></span>
                    <span><Link to="/dialogs">Dialogs</Link></span>
                </div>
                <div className="container" id="body">
                    <div id="main">
                        <div className="jumbotron text-center">
                            <h1 id="comp_name">Not found</h1>
                        </div>
                        <div className="container content">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

const NotFound = new React.createClass({
    getInitialState: function () {
        return {
              name: "Not found"
        };
    },
    componentDidMount: function () {
        document.getElementById("comp_name").textContent = this.state.name;
    },
    render: function () {
        return (
            <div className="not_found">
                <div className="center-block">
                    <h1>WOOOOPS... Sorry, I can't found that</h1>
                </div>
            </div>
        )
    }
});

const routes = {
    path:'/',
    component: App,
    indexRoute: {component: News},
    childRoutes: [
        {path:'wars', component: Wars},
        {path:'/users/:id', component: Users},
        {path:'*', component: NotFound},
    ]
};

ReactDOM.render(
  <Router history={browserHistory} routes={routes}/>,
    document.getElementById('root')
);

// ReactDOM.render(
//     <App    />,
//     document.getElementById('root')
// );