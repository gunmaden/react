/**
 * Created by gunmaden on 05.03.17.
 */

import React from 'react'
import {Router, Link, Route, IndexRoute, IndexLink, browserHistory} from 'react-router'
import axios from 'axios'

const Dispute = React.createClass({
    propTypes: {
        data: React.PropTypes.shape({
            Initiator: React.PropTypes.object.isRequired,
            Opponent: React.PropTypes.object.isRequired,
            leftPercents: React.PropTypes.number.isRequired,
            rightPercents: React.PropTypes.number.isRequired,
            Header: React.PropTypes.string.isRequired,
        })
    },
    getInitialState: function () {
        return {
            visble: false
        };
    },
    render: function () {
        var Initiator = this.props.data.Initiator.username,
            Opponent = this.props.data.Opponent.username,
            Header = this.props.data.Header,
            leftPercents = this.props.data.leftPercents,
            rightPercents = this.props.data.rightPercents;
        return (
            <div className="Dispute page-header">
                <h1 className="opponents">
                    <Link to={`/users/${this.props.data.Initiator.id}`} activeClassName="active">
                        {Initiator}
                        </Link>
                    &nbsp; VS &nbsp;
                    <Link to={`/users/${this.props.data.Opponent.id}`} activeClassName="active">
                        {Opponent}
                        </Link>
                </h1>
                <h1>
                    {Header}
                </h1>
                <div className="progress">
                    <div className="progress-bar progress-bar-success active"
                         role="progressbar"
                         style = {{ width: leftPercents + '%' }}>
                        {leftPercents}%
                    </div>
                    <div className="progress-bar progress-bar-danger active"
                         role = "progressbar"
                         style = {{ width: rightPercents +'%' }}>
                        {rightPercents}%
                    </div>
                </div>
            </div>
        )
    }
});

const Wars = React.createClass({

    loadWarsFromServer: function () {
        axios.get('/api/wars')
            .then(function (response) {
                this.setState({Wars: response.data});
            }.bind(this))
            .catch(function (error) {
                console.log(error);
            });
    },
    getInitialState: function () {
        return {
            Wars: [],
            name: "Wars"
        };
    },
    componentDidMount: function () {
        this.loadWarsFromServer();
        document.getElementById("comp_name").textContent = this.state.name;
    },
    render: function () {
        let data = this.state.Wars;
        let disputeTemplate;
        if (data.length != 0) {
            disputeTemplate = data.map(function (item, index) {
                return (
                    <div key={index}>
                        <Dispute data={item}/>
                    </div>
                )
            })
        }
        else {
            return (
                disputeTemplate = (
                    <div className="no-data">
                        <div className="row">
                            <div className="text-center">Cпоров пока нет</div>
                        </div>
                    </div>)
            )
        }
        return (
            <div className="content Wars">
                {disputeTemplate}
                <strong className={'Wars_count ' + (data.length > 0 ? '' : 'none')}>
                    Всего споров: {data.length}
                </strong>
            </div>
        );
    }
});

export {Wars};