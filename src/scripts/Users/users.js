/**
 * Created by gunmaden on 05.03.17.
 */

import React from 'react'
import axios from 'axios'
import Moment from 'react-moment';

const User = React.createClass({
    propTypes: {
        data: React.PropTypes.shape({
            username: React.PropTypes.string.isRequired,
            City: React.PropTypes.string.isRequired,
            Age: React.PropTypes.number.isRequired,
            LastDateSeen: React.PropTypes.string.isRequired
        })
    },
    render: function () {
        var username = this.props.data.username,
            City = this.props.data.City,
            Age = this.props.data.Age,
            LastDateSeen = this.props.data.LastDateSeen;

        return (
            <div className="user">
                <h1 className="username">
                    {username}
                </h1>
                <hr/>
                <h3 className="City">
                    City:{City}
                </h3>
                <h3 className="Age">
                    Age:{Age}
                </h3>
                <h3 className="LastSeen">
                    Last seen: <Moment locale="ru" format='DD.MM.YYYY'>{LastDateSeen}</Moment>
                </h3>
            </div>
        )
    }
});

const Users = React.createClass({
    loadUsersFromServer: function (id) {
        axios.get('/api/users/' + id)
            .then(function (response) {
                this.setState({Users: [response.data]});
            }.bind(this))
            .catch(function (error) {
                console.log(error);
            });
    },
    getInitialState: function () {
        return {
            Users: [],
            name: "User"
        };
    },
    componentDidMount: function () {
        this.loadUsersFromServer(this.props.params.id);
        document.getElementById("comp_name").textContent = this.state.name;
    },
    render: function () {
        let data = this.state.Users;
        let usersTemplate;
        if (data.length != 0) {
            usersTemplate = data.map(function (item, index) {
                return (
                    <div key={index}>
                        <User data={item}/>
                    </div>
                )
            })
        }
        else {
            return (
                usersTemplate = (
                    <div className="no-data">
                        <div className="row">
                            <div className="text-center">Данный пользователь не найден</div>
                        </div>
                    </div>)
            )
        }
        return (
            <div className="content Users">
                {usersTemplate}
            </div>
        );
    }
});

export {Users};