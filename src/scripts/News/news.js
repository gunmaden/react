/**
 * Created by gunmaden on 05.03.17.
 */
import React from 'react'
import axios from 'axios'

const Article = React.createClass({
    propTypes: {
        data: React.PropTypes.shape({
            author: React.PropTypes.string.isRequired,
            text: React.PropTypes.string.isRequired,
            bigText: React.PropTypes.string.isRequired
        })
    },
    readMore: function (e) {
        e.preventDefault();
        this.setState(
                {
                    visible: true
                }
            );
    },
    getInitialState: function () {
        return {
            visble: false
        };
    },
    render: function () {
        var author = this.props.data.author,
            text = this.props.data.text,
            bigText = this.props.data.bigText,
            visible = this.state.visible;
        return (
            <div className="article page-header">
                <h1 className="news_author">
                    {author}:
                </h1>
                <h3 className="news_text"> {text}
                </h3>
                <a href="#"
                   onClick={this.readMore}
                   className={'news_read_more	' + (visible ? 'none' : '')}>
                    Подробнее
                </a>
                <p className={'news_big-text ' + (visible ? '' : 'none')}>
                    {bigText}
                </p>
            </div>
        )
    }
});

const News = React.createClass({

    loadNewsFromServer: function () {
        axios.get('/api/news')
            .then(function (response) {
                this.setState({news: response.data});
            }.bind(this))
            .catch(function (error) {
                console.log(error);
            });
    },
    getInitialState: function () {
        return {
            counter: 0,
            news: [],
            name: "News"
        };
    },
    componentDidMount: function () {
        this.loadNewsFromServer();
        document.getElementById("comp_name").textContent = this.state.name;
        // setInterval(this.loadNewsFromServer, this.props.pollInterval);
    },

    render: function () {
        let data = this.state.news;
        let newsTemplate;
        if (data.length != 0) {
            newsTemplate = data.map(function (item, index) {
                return (
                    <div key={index}>
                        <Article data={item}/>
                    </div>
                )
            })
        }
        else {
            return (
                newsTemplate = (
                    <div className="no-data">
                        <div className="row">
                            <div className="text-center">Новостей пока нет</div>
                        </div>
                    </div>)
            )
        }
        return (
            <div className="content News">
                {newsTemplate}
                <strong className={'news_count ' + (data.length > 0 ? '' : 'none')}>
                    Всего новостей: {data.length}
                </strong>
            </div>
        );
    }
});

export {News};