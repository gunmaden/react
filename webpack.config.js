/**
 * Created by gunmaden on 02.03.17.
 */

'use strict'; // eslint-disable-line

const webpack = require('webpack');
const CleanPlugin = require('clean-webpack-plugin');
const merge = require('webpack-merge');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');

// const sourceMapQueryStr = '-sourceMap';
const sourceMapQueryStr = '';
const watcher = false;

let webpackConfig = {

    entry: {
        'bundle.js':['./src/scripts/app.js'],
        'bundle.css':['./src/styles/app.scss'],
    },
    output: {
        path: path.resolve('./dist'),
        filename: `[name]`,
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js?$/,
                include: './src/scripts/**',
                loader: 'eslint',
                query:{
                    cache: true
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel",
                query: {
                    cacheDirectory: true
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallbackLoader: 'style',
                    loader: [
                        `css?${sourceMapQueryStr}`,
                        'postcss',
                        `resolve-url?${sourceMapQueryStr}`,
                        `sass?${sourceMapQueryStr}`,
                        {
                            loader:'sass-resources',
                            options:{
                                resources: './src/styles/**/*.scss'
                            }
                        },
                    ],
                }),

            },
            {
                test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg)$/,
                include: /node_modules/,
                loader: 'file',
                options: {
                    name: `vendor/fonts/[name].[ext]`,
                },
            },
        ],
    },
    resolve: {
        modules: [
            'src',
            'node_modules',
        ],
        enforceExtension: false,
    },
    resolveLoader: {
        moduleExtensions: ['-loader'],
    },
    plugins: [
        new CleanPlugin(['./dist'], {
            verbose: false,
        }),
        new ExtractTextPlugin({
            filename: `[name]`,
            disable: watcher,
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: false,
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.scss$/,
            options: {
                output: { path: './dist' },
                postcss: [
                    autoprefixer({ browsers: [] }),
                ],
            },
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.js$/,
            options: {
                eslint: { failOnWarning: false, failOnError: true },
            },
        }),
    ],
};

if (watcher) {
    // webpackConfig.entry = require('./util/addHotMiddleware')(webpackConfig.entry);
    webpackConfig = merge(webpackConfig, require('./webpack.config.watch'));
}

module.exports = webpackConfig;
